@extends('layouts.app')

@section('content')
    <section class="Form mx-4 my-5">
        <div class="container">
            <div class="accordion" id="accordionExample">

                {{-- QUERY #1 --}}
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Query 1
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Give all the customers who lives in Canada and sort by customer_id.
                            <table class="table table-striped table-bordered" id="tableQueryOne">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer ID</th>
                                        <th>Customer City</th>
                                        <th>Customer Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($customers) > 0)
                                        @foreach($customers as $customer)
                                            @if(!empty($customer))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $customer->cust_id }}</td>
                                                <td>{{ $customer->city }}</td>
                                                <td>{{ $customer->first_name }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #1 --}}

                {{-- QUERY #2 --}}
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                Query 2
                            </button>
                        </h2>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            List all different customers who made bookings.
                            <table class="table table-striped table-bordered" id="tableQuerySeven">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking Number</th>
                                        <th>Customer ID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bookings) > 0)
                                        @foreach($bookings as $booking)
                                            @if(!empty($booking))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $booking->booking_no }}</td>
                                                <td>{{ $booking->cust_id }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #2 --}}

                {{-- QUERY #3 --}}
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                Query 3
                            </button>
                        </h2>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            Display all currency exchange rate is greater than 1. Please sort them by from_currency and to_currency.
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #3 --}}

                {{-- QUERY #4 --}}
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                Query 4
                            </button>
                        </h2>
                    </div>

                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            List all the flight availabilities between Toronto (airport code is 'YYZ') and New York (airport code is 'JFK'). Please display flight_no, origin, destination, depature_time, and arrival_time. Please sort them by flight_no.
                            <table class="table table-striped table-bordered" id="tableQueryFour">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Flight No.</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Departure Time</th>
                                        <th>Arrival Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($listFlightAv) > 0)
                                        @foreach($listFlightAv as $flightAv)
                                            @if(!empty($flightAv))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $flightAv->flight_no }}</td>
                                                <td>{{ $flightAv->origin }}</td>
                                                <td>{{ $flightAv->destination }}</td>
                                                <td>{{ $flightAv->departure_time }}</td>
                                                <td>{{ $flightAv->arrival_time }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #4 --}}

                {{-- QUERY #5 --}}
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                Query 5
                            </button>
                        </h2>
                    </div>

                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            List all customers who did not place any booking. Please display customer_id only, and sort records by customer_id.
                            <table class="table table-striped table-bordered" id="tableQueryFive">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer ID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($listCustomer) > 0)
                                        @foreach($listCustomer as $customer)
                                            @if(!empty($customer))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $customer->cust_id }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #5 --}}

                {{-- QUERY #6 --}}
                <div class="card">
                    <div class="card-header" id="headingSix">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                Query 6
                            </button>
                        </h2>
                    </div>

                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                            Display all customer's first_name, last_name, phone_no (format like 416-111-2222) and email. Please sort them by customer_id.
                            <table class="table table-striped table-bordered" id="tableQuerySix">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone Number</th>
                                        <th>Email Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($listCustomer) > 0)
                                        @foreach($listCustomer as $customer)
                                            @if(!empty($customer))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $customer->cust_id }}</td>
                                                <td>{{ $customer->first_name }}</td>
                                                <td>{{ $customer->last_name }}</td>
                                                <td>{{ $customer->phone_number }}</td>
                                                <td>{{ $customer->email }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #6 --}}

                {{-- QUERY #7 --}}
                <div class="card">
                    <div class="card-header" id="headingSeven">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                Query 7
                            </button>
                        </h2>
                    </div>

                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                        <div class="card-body">
                            List all canceled bookngs. please display booking_no, customer_id, flight_no, origin, destination, class, status, and booking_city. Please also sort by booking_no, customer_id and flight_no.
                            <table class="table table-striped table-bordered" id="tableQuerySeven">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking Number</th>
                                        <th>Customer ID</th>
                                        <th>Flight Number</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Class</th>
                                        <th>Status</th>
                                        <th>Booking City</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bookings) > 0)
                                        @foreach($bookings as $booking)
                                            @if(!empty($booking))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $booking->booking_no }}</td>
                                                <td>{{ $booking->cust_id }}</td>
                                                <td>{{ $booking->flight_no }}</td>
                                                <td>{{ $booking->origin }}</td>
                                                <td>{{ $booking->destination }}</td>
                                                <td>{{ $booking->class }}</td>
                                                <td>{{ $booking->status }}</td>
                                                <td>{{ $booking->booking_city }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #7 --}}

                {{-- QUERY #8 --}}
                <div class="card">
                    <div class="card-header" id="headingEight">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                Query 8
                            </button>
                        </h2>
                    </div>

                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                        <div class="card-body">
                            List total_price, total_payment and total_balance for each city. Please exclude canceled bookings and sort records by city_name.
                            <table class="table table-striped table-bordered" id="tableQueryEight">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>City Name</th>
                                        <th>Total Price</th>
                                        <th>Total Payment</th>
                                        <th>Total Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bookings) > 0)
                                        @foreach($bookings as $booking)
                                            @if(!empty($booking))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $booking->city_name }}</td>
                                                <td>{{ $booking->total_price }}</td>
                                                <td>{{ $booking->total_payment }}</td>
                                                <td>{{ $booking->total_balance }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #8 --}}

                {{-- QUERY #9 --}}
                <div class="card">
                    <div class="card-header" id="headingNine">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                                Query 9
                            </button>
                        </h2>
                    </div>

                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                        <div class="card-body">
                            Calculate new total_price for each booking if origin airport tax increase by 0.01 and destination airport tax decrease by 0.005. Please display booking_no, origin, destination, flight_price, previous_total_price and new_total_price.
                            <table class="table table-striped table-bordered" id="tableQueryNine">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking Number</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Flight Price</th>
                                        <th>Previous Total Price</th>
                                        <th>New Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bookings) > 0)
                                        @foreach($bookings as $booking)
                                            @if(!empty($booking))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $booking->booking_number }}</td>
                                                <td>{{ $booking->origin }}</td>
                                                <td>{{ $booking->destination }}</td>
                                                <td>{{ $booking->flight_price }}</td>
                                                <td>{{ $booking->previous_flight_price }}</td>
                                                <td>{{ $booking->previous_flight_price }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #9 --}}

                {{-- QUERY #10 --}}
                <div class="card">
                    <div class="card-header" id="headingTen">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                                Query 10
                            </button>
                        </h2>
                    </div>

                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample">
                        <div class="card-body">
                            List number_of_bookings, number_of_emails, number_of_phones and number_of_faxs for each customer.
                            <table class="table table-striped table-bordered" id="tableQueryTen">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer ID</th>
                                        <th>Number of Bookings</th>
                                        <th>Number of Emails</th>
                                        <th>Number of Phones</th>
                                        <th>Number of Faxs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($customers) > 0)
                                        @foreach($customers as $customer)
                                            @if(!empty($customer))
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $customer->cust_id }}</td>
                                                <td>{{ $customer->total_bookings }}</td>
                                                <td>{{ $customer->total_emails }}</td>
                                                <td>{{ $customer->total_phones }}</td>
                                                <td>{{ $customer->total_faxs }}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- END OF QUERY #10 --}}

            </div>
        </div>
    </section>

    <script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
    </script>
@endsection