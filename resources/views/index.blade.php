@extends('layouts.app')

@section('content')
    <section class="Form mx-4 my-5">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-lg-5 box">
                    <img src="{{asset('assets/img/flight.jpg')}}" class="img-fluid img-form" alt="Photo by Sheila from Pexels">
                </div>
                <div class="col-lg-7 px-4 pt-4">
                    <form action="#">
                        <h2>Airline Reservation</h2>

                        {{-- Nama pelanggan --}}
                        <div class="form-group" style="padding-top: 16px">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold" for="firstName">First Name</label>
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold" for="lastName">Last Name</label>
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
                                </div>
                            </div>
                        </div>

                        {{-- Alamat email pelanggan --}}
                        <div class="form-group">
                            <label class="font-weight-bold" for="emailAddress">Email Address</label>
                            <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="Email Address">
                        </div>

                        {{-- Informasi alamat pelanggan --}}
                        <h5 class="font-weight-bold" style="padding-top: 8px">Mailing Address Information</h5>
                        <div class="form-group">
                            <label class="font-weight-bold" for="street">Street</label>
                            <input type="text" class="form-control" id="street" name="street" placeholder="Street">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" placeholder="City">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="province">Province or State</label>
                            <input type="text" class="form-control" id="province" name="province" placeholder="Province or State">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold" for="country">Country</label>
                                    <input type="text" class="form-control" id="country" name="country" placeholder="Country">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold" for="postalCode">Postal Code</label>
                                    <input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Postal Code">
                                </div>
                            </div>
                        </div>

                        {{-- Informasi nomor telepon pelanggan --}}
                        <h5 class="font-weight-bold" style="padding-top: 8px">Phone Number Information</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold" for="phoneCountry">Phone Country Code</label>
                                    <input type="number" class="form-control" id="phoneCountry" name="phoneCountry" placeholder="Phone Country Code">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold" for="phoneArea">Phone Area Code</label>
                                    <input type="number" class="form-control" id="phoneArea" name="phoneArea" placeholder="Phone Area Code">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="phoneNumber">Phone Number</label>
                            <input type="number" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Phone Number">
                        </div>

                        {{-- Informasi nomor fax pelanggan --}}
                        <h5 class="font-weight-bold" style="padding-top: 8px">Fax Number Information</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold" for="faxCountry">Fax Country Code</label>
                                    <input type="number" class="form-control" id="faxCountry" name="faxCountry" placeholder="Fax Country Code">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold" for="faxArea">Fax Area Code</label>
                                    <input type="number" class="form-control" id="faxArea" name="faxArea" placeholder="Fax Area Code">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="faxNumber">Fax Number</label>
                            <input type="number" class="form-control" id="faxNumber" name="faxNumber" placeholder="Fax Number">
                        </div>

                        {{-- <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">First Name</label>
                                    <input type="email" placeholder="First Name" class="form-control my-2 p-4">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Last Name</label>
                                    <input type="email" placeholder="Last Name" class="form-control my-2 p-4">
                                </div>
                            </div>
                        </div>

                        <h5 class="font-weight-bold mt-2 mb-0">Mailing Address Information</h5>
                        <div class="form-group">
                            <label class="font-weight-bold mt-2 mb-0">Street</label>
                            <input type="email" placeholder="Street" class="form-control my-2 p-4">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold mt-2 mb-0">City</label>
                            <input type="email" placeholder="City" class="form-control my-2 p-4">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold mt-2 mb-0">Province or State</label>
                            <input type="email" placeholder="Province or State" class="form-control my-2 p-4">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Country</label>
                                    <input type="email" placeholder="Country" class="form-control my-2 p-4">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Postal Code</label>
                                    <input type="email" placeholder="Postal Code" class="form-control my-2 p-4">
                                </div>
                            </div>
                        </div>

                        <h5 class="font-weight-bold mt-2 mb-0">Phone Number Information</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Phone Country Code</label>
                                    <input type="email" placeholder="Phone Country Code" class="form-control my-2 p-4">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Phone Area Code</label>
                                    <input type="email" placeholder="Phone Area Code" class="form-control my-2 p-4">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold mt-2 mb-0">Phone Number</label>
                            <input type="email" placeholder="Phone Number" class="form-control my-2 p-4">
                        </div>

                        <h5 class="font-weight-bold mt-2 mb-0">Fax Number Information</h5>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Fax Country Code</label>
                                    <input type="email" placeholder="Fax Country Code" class="form-control my-2 p-4">
                                </div>
                                <div class="col">
                                    <label class="font-weight-bold mt-2 mb-0">Fax Area Code</label>
                                    <input type="email" placeholder="Fax Area Code" class="form-control my-2 p-4">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold mt-2 mb-0">Fax Number</label>
                            <input type="email" placeholder="Fax Number" class="form-control my-2 p-4">
                        </div> --}}

                        <div class="form-group">
                            <button type="button" class="btn btn-cust">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection